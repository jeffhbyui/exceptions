package exceptionhandling;

import java.util.*;

public class ExceptionHandling {

    public static double divideNumbers(int num1, int num2) throws Exception {
        if (num2 == 0) {
            throw new Exception("Enter a non-zero number: ");
        }
        return num1 / num2;
    }



    public static void main(String [] args) throws Exception {

        int num1 = 0;
        int num2 = 0;

        Scanner console = new Scanner(System.in);
        do {
            System.out.print("Enter first number: ");
            num1 = console.nextInt();
            System.out.print("Enter second number: ");
            num2 = console.nextInt();
            if(num2 == 0){
                System.out.println("Enter a second number that is not zero");
            }
        }while(num2 == 0);
           System.out.println("Quotient is: " + divideNumbers(num1, num2));
        }
    }
