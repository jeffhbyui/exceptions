package exceptionhandling;

import org.junit.Assert;
import org.junit.Test;

// Note: Only one assertion per test, for clarity in understanding failures and purpose of tests

public class Tests {
    // Test an exposed public method of the ExceptionHandling class
    @Test
    public void testDivision() throws Exception {
        ExceptionHandling fn = new ExceptionHandling();
        double answer = fn.divideNumbers(6, 2);
        Assert.assertTrue(3 == answer);
    }

    // Test that we surface a user-friendly error when a zero is passed as the second number
    @Test
    public void testDivisionException() throws Exception {
        ExceptionHandling fn = new ExceptionHandling();
        Exception exception = Assert.assertThrows(Exception.class, () -> fn.divideNumbers(1,0));

        String expectedError = "Enter a non-zero number:";
        expectedError.stripTrailing();
        String actualError = exception.getMessage();
        Assert.assertTrue(actualError.contains(expectedError));
    }
}
